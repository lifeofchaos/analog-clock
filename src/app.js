import 'normalize.css';
import './app.css';

const manillaSegundos = document.querySelector('.segundos');
const manillaMinutos = document.querySelector('.minutos');
const manillaHoras = document.querySelector('.horas');

function init() {
    const marcasReloj = document.querySelectorAll('.marcas_reloj li');
    // 360º / 60 = 6, por tanto cada marca tiene 6 grados de rotación
    let rotacion = 6;
    marcasReloj.forEach(marca => {
        marca.style.transform = `rotate(${rotacion}deg) translateY(140px)`;
        rotacion += 6;
    });
    setInterval(() => {
        let hoy = new Date();
        let segundos = hoy.getSeconds();
        let minutos = hoy.getMinutes();
        let horas = hoy.getHours();
        // 0.5 es porque el ancho del segundero es de 1px, para ajustarlo un poco
        // sumo 0.5, pero no es necesaria precisión en este ejericicio
        manillaSegundos.style.transform = `rotate3d(0,0,1, ${ 6 * segundos + .8 }deg)`;
        manillaMinutos.style.transform = `rotate3d(0, 0, 1, ${ 6 * minutos }deg)`;
        // Las horas son 12 por tanto 360 / 12 = 30 grados por hora
        // Si los segundos no son 0 estos grados aumentan 30 / segundos grados
        let aumento = 0;
        if (segundos !== 0) {
            aumento = 30 / 60;
        }
        manillaHoras.style.transform = `rotate3d(0, 0, 1, ${30 * horas + aumento }deg)`;
    }, 500);
}

init();